import { memo, useState, useEffect } from 'react';
import { Grid, ListItem, ListItemText, Typography, Avatar, Checkbox } from '@material-ui/core';

const ContactItem = (props) => {
  const [checked, setChecked] = useState(false);
  const { onChange, user } = props;
  const { id, avatar, first_name, last_name } = user;

  useEffect(() => {
    onChange(id, checked);
  }, [checked])

  return (
    <>
      <Grid item style={{ width: '100%' }}>
        <ListItem>
          <Avatar alt="user" src={avatar} />
          <ListItemText>
            <Typography>
              {first_name}
            </Typography>
            <Typography>
              {last_name}
            </Typography>
          </ListItemText>
          <Checkbox
            id={id.toString()}
            checked={checked}
            onChange={() => setChecked(!checked)} />
        </ListItem>
      </Grid>
    </>
  );
}

export default memo(ContactItem, (prevProps, nextProps) => {
  return (prevProps.user && prevProps.user.id) === (nextProps.user && nextProps.user.id);
});