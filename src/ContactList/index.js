import ContactItem from './ContactItem';
import { List, Grid } from '@material-ui/core';


const ContactList = ({ users, onToggle, loading, error }) => {
  if (error) {
    return <div>{error}</div>
  }
  else if (loading) {
    return <div>Loading</div>
  }
  return (
    <Grid
      container
      direction="column"
      justify="center"
      alignItems="center"
    >
      <List style={{ width: '100%' }}>
        {users.map(user => (
          <ContactItem
            onChange={onToggle}
            key={user.id}
            user={user} />
        ))}
      </List>
    </Grid>
  );
}

export default ContactList;
