import { useState, useEffect } from 'react';
import { Typography, AppBar } from '@material-ui/core';
import ContactList from '../ContactList/index.js'
import Search from '../Search/index.js';
import debounce from 'lodash.debounce';

const DEBOUNCE_BREAK = 500;
const URL = 'https://teacode-recruitment-challenge.s3.eu-central-1.amazonaws.com/users.json';

function Root() {

  const [users, setUsers] = useState([]);
  const [loading, setLoading] = useState(true)
  const [error, setError] = useState(null)
  const [filteredUsers, setFilteredUsers] = useState(users);
  const [checked, setChecked] = useState({});
  const [lastChecked, setLastChecked] = useState(null);
  const [search, setSearch] = useState('');
  const delayedSearch = debounce(q => {
    if (q !== "") {
      const newUsers = users
        .filter(({ id }) => !!checked[id])
        .filter(({ first_name, last_name }) => `${first_name}${last_name}`.toLowerCase().includes(q));
      setFilteredUsers(newUsers);
    } else {
      setFilteredUsers(users);
    }
  }, DEBOUNCE_BREAK);

  useEffect(() => {
    fetch(URL)
      .then(response => {
        if (response.ok) {
          return response;
        }
        else {
          throw new Error(`something went wrong, code: ${response.status}`);
        }
      })
      .then(response => response.json())
      .then(response => response.sort((a, b) => {
        let firstLastName = a.last_name.toLowerCase().split('')[0];
        let secondLastName = b.last_name.toLowerCase().split('')[0];
        if (firstLastName < secondLastName) return -1;
        if (firstLastName > secondLastName) return 1;
        return 0;
      }))
      .then(result => {
        setSearch("");
        setUsers(result);
        setFilteredUsers(result);
        setLoading(false)
      })
      .catch(error => {
        setError(error.message)
      })
  }, []);

  useEffect(() => {
    if (lastChecked) {
      const newChecked = { ...checked };
      const { value } = lastChecked;
      const { id } = lastChecked;
      if (!!newChecked[id] !== value) {
        newChecked[id] = value;
        setChecked(newChecked);
      }
    }
  }, [lastChecked])

  useEffect(() => {
    console.log(`users checked`, checked);
  }, [checked])

  const handleToggle = (id, value) => {
    setLastChecked({ id, value });
  }

  const handleSearch = (e) => {
    setSearch(e.target.value);
    delayedSearch(e.target.value);
  };

  return (
    <div style={{ padding: '10px' }}>
      <AppBar
        style={{ height: '50px' }}
        position="relative">
        <Typography
          variant="h6"
          style={{
            lineHeight: '2.1',
            textAlign: 'center',
          }}>Contacts</Typography>
      </AppBar>
      <Search
        value={search}
        onChange={handleSearch}
      />
      <ContactList
        users={filteredUsers}
        loading={loading}
        error={error}
        onToggle={handleToggle}
      />
    </div>
  );
}

export default Root;
