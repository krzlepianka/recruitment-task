import { TextField } from '@material-ui/core';

const Search = ({ value, onChange }) => {
  return (
    <TextField
        style={{ width: '100%', padding: '20px 0px', textAlign: 'center' }}
        placeholder="search"
        type="string"
        value={value}
        onChange={onChange}/>
  );
}

export default Search;
