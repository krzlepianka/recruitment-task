const users = [
    {
        id: 1,
        first_name: 'Jack',
        last_name: 'Wick',
        email: 'test@o2.pl',
        gender: 'male',
        avatar: 'https://i.kinja-img.com/gawker-media/image/upload/t_original/ijsi5fzb1nbkbhxa2gc1.png'
    },
    {
        id: 2,
        first_name: 'Jack',
        last_name: 'Wolfram',
        email: 'test2@o2.pl',
        gender: 'male',
        avatar: 'https://www.nj.com/resizer/h8MrN0-Nw5dB5FOmMVGMmfVKFJo=/450x0/smart/cloudfront-us-east-1.images.arcpublishing.com/advancelocal/SJGKVE5UNVESVCW7BBOHKQCZVE.jpg'
    }
];

export default users;
